from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import psycopg2


def set_chrome_options():

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    chrome_options.experimental_options["prefs"] = chrome_prefs
    chrome_prefs["profile.default_content_settings"] = {"images": 2}
    return chrome_options


while True:

    try:
        conn = psycopg2.connect(
            database="opticdb",
            user="postgres",
            password="Reeve123",
            host="127.0.0.1",
            port="5432",
        )
        curs = conn.cursor()
        curs.execute(
            "SELECT * FROM postcodes WHERE fetched IS FALSE AND scraped IS FALSE LIMIT 1;"
        )

        fetch = curs.fetchone()

        postcode = fetch[1]
        uid = fetch[0]

        curs.execute(f"""update postcodes set fetched = true where id = '{uid}'""")

        driver = webdriver.Chrome(
            executable_path="/usr/local/bin/chromedriver", options=set_chrome_options()
        )

        driver.get("https://gigabitvoucher.culture.gov.uk/")

        driver.find_element_by_xpath('//*[@id="postcode"]').send_keys(postcode)

        driver.find_element_by_xpath(
            "/html/body/div/main/section[3]/div/div/div[2]/div/form/input[3]"
        ).click()

        driver.implicitly_wait(1)
        driver.find_element_by_xpath('//select[@id="address"]').click()
        driver.find_element_by_xpath('//*[@id="address"]/option[2]').click()
        driver.implicitly_wait(1)

        driver.find_element_by_xpath(
            "/html/body/div/main/section[3]/div/div/div[1]/p[3]"
        ).click()

        res = driver.find_element_by_xpath(
            "/html/body/div/main/section[3]/div/div/div[3]/p[1]"
        )
        res = res.text

        if "currently eligible" in res:
            val = True
        elif "not eligible" in res:
            val = False

        curs.execute(
            f"""update postcodes set scraped = true, avail = {val} where id = '{uid}';"""
        )

        driver.close()
        conn.commit()
        conn.close()

    except Exception as e:
        print(e)
        curs.execute(f"""update postcodes set error = true where id = '{uid}';""")
        driver.close()
        conn.commit()
        conn.close()
